import "reflect-metadata";
import { createConnection } from "typeorm";
import express from "express";
import businessRoutes from "./routes/businessRoutes";
import reviewRoutes from "./routes/reviewRoutes";

createConnection().then(async () => {
  const app = express();
  const PORT = process.env.PORT || 3000;

  app.use(express.json());
  app.use('/business', businessRoutes);
  app.use('/review', reviewRoutes);

  app.listen(PORT, () => {
    console.log(`Servidor corriendo en http://localhost:${PORT}`);
  });
}).catch(error => console.log(error));
