import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity()
export class Business {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 75 })
  name: string;

  @Column({ nullable: true })
  website?: string;

  @Column({ nullable: true })
  address?: string;

  @Column({ nullable: true })
  phone?: string;

  @Column()
  email: string;

  @Column()
  numberOfReviews: number;
}
