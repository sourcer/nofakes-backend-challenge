import { Router } from 'express';

const router = Router();

// Endpoint para publicar una revisión
router.post('/', (req, res) => {
  // Implementación aquí...
});

// Endpoint para obtener revisiones por ID del negocio
router.get('/:businessId', (req, res) => {
  // Implementación aquí...
});

export default router;
