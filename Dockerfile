# Usar imagen oficial de Node
FROM node:14

# Crear una carpeta para la app en el contenedor
WORKDIR /usr/src/app

# Copiar package.json y package-lock.json
COPY package*.json ./

# Instalar las dependencias
RUN npm install

# Copiar el resto del código fuente de la aplicación
COPY . .

# Exponer el puerto en el que tu aplicación corre
EXPOSE 3000

# Comando para correr la aplicación
CMD [ "node", "dist/server.js" ]
